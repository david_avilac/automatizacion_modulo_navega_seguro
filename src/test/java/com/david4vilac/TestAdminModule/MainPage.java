package com.david4vilac.TestAdminModule;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

//Listar los componentes necesarios para el módulo
public class MainPage {


    @FindBy(xpath = "//*[@id=\"tab-button-settings\"]/ion-icon")
    public WebElement toolsMenu;


    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
