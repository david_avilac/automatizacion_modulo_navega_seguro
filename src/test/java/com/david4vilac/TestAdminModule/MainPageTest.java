package com.david4vilac.TestAdminModule;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class MainPageTest {
    private WebDriver driver;
    private MainPage mainPage;


    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();

        driver = new ChromeDriver();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--user-agent=iPhone SE");
        org.openqa.selenium.Dimension d = new org.openqa.selenium.Dimension(500, 850);
        driver.manage().window().setSize(d);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://localhost:8100/menu/location");

        mainPage = new MainPage(driver);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    //Validar los estados de API KEYS
    @Test
    public void validatorAPI() throws InterruptedException {
        mainPage.toolsMenu.click();
        WebElement elemento = driver.findElement(By.xpath("//*[@id=\"footer\"]/ion-item[2]/a"));
        Thread.sleep(1000);
        elemento.click();

        WebElement elementHeader = driver.findElement(By.xpath("//ion-card-header[.//ion-card-title[text()='APIS']]"));
        Thread.sleep(1000);
        elementHeader.click();

        //WebElement elementApiOne = driver.findElement(By.xpath("//html/body/app-root/ion-app/ion-router-outlet/app-api-key-screen/ion-content/ion-card[1]/ion-card-header/img"));
        WebElement elementApiOne = driver.findElement(By.xpath("html/body/app-root/ion-app/ion-router-outlet/app-api-key-screen/ion-content/ion-card[2]/ion-card-header"));
        Thread.sleep(1000);
        elementApiOne.click();

        Thread.sleep(1000);
        WebElement chipEstado = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-api-form/ion-content/ion-card/ion-card-content/ion-item/ion-label/ion-chip"));
        String estado = chipEstado.getText();
        System.out.println("El estado es: " + estado);

        String estadoActual = chipEstado.getText();
        //String actualUrl = driver.getCurrentUrl();

        if (estadoActual.equals("200")) {
            System.out.println("El estado: " + estadoActual);
        } else {
            System.out.println("El estado no es el esperado: "+ 200);
            System.out.println("CAmbiando API ...");
            WebElement inputElement = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-api-form/ion-content/ion-card/ion-card-content/form/ion-item/ion-input/input"));
            inputElement.clear();
            inputElement.sendKeys("Texto a escribir");

            WebElement buttonUpdate = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-api-form/ion-content/ion-card/ion-card-content/ion-button"));
            buttonUpdate.click();

            WebElement okAlert = driver.findElement(By.xpath("//*[@id=\"ion-overlay-2\"]/div[2]/div[3]/button[2]/span"));
            okAlert.click();

            WebElement newChipEstado = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-api-form/ion-content/ion-card/ion-card-content/ion-item/ion-label/ion-chip"));
            System.out.println("Nuevo estado de API: " + newChipEstado.getText());
        }


    }

    //Estado base de datos
    @Test
    public void validatorDataBases() throws InterruptedException {
        mainPage.toolsMenu.click();
        WebElement elemento = driver.findElement(By.xpath("//*[@id=\"footer\"]/ion-item[2]/a"));
        Thread.sleep(1000);
        elemento.click();

        WebElement elementHeader = driver.findElement(By.xpath("//ion-card-header[.//ion-card-title[text()='BASES DE DATOS']]"));
        Thread.sleep(1000);
        elementHeader.click();
        WebElement elementChpOne = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-database-view/ion-content/ion-card[1]/ion-card-content/div[1]/ion-label/ion-chip"));
        WebElement elementChpTwo = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-database-view/ion-content/ion-card[2]/ion-card-content/div[1]/ion-label/ion-chip"));
        WebElement elementChpThree = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-database-view/ion-content/ion-card[3]/ion-card-content/div[1]/ion-label/ion-chip"));

        System.out.println("Nuevo estado de DataBase 1: " + elementChpOne.getText());
        System.out.println("Nuevo estado de DataBase 2: " + elementChpTwo.getText());
        System.out.println("Nuevo estado de DataBase 3: " + elementChpThree.getText());

    }


    //Filtro de usuarios
    @Test
    public void validatorUsers() throws InterruptedException {
        mainPage.toolsMenu.click();
        WebElement elemento = driver.findElement(By.xpath("//*[@id=\"footer\"]/ion-item[2]/a"));
        Thread.sleep(1000);
        elemento.click();

        WebElement elementHeader = driver.findElement(By.xpath("//ion-card-header[.//ion-card-title[text()='USUARIOS']]"));
        Thread.sleep(1000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", elementHeader);

        elementHeader.click();
        Thread.sleep(3000);

        WebElement seachElement = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-user-name-screen/ion-content/ion-item/ion-input/input"));
        seachElement.sendKeys("david_avilachotmail.com");
        Thread.sleep(3000);
    }

    //Cambio de rol usuario.
    @Test
    public void changeRolUser() throws InterruptedException {

        validatorUsers();
        WebElement changeElement = driver.findElement(By.xpath("//ion-icon[@tabindex='0']"));
        changeElement.click();
        Thread.sleep(3000);

        WebElement rolElement = driver.findElement(By.xpath("//html/body/app-root/ion-app/ion-router-outlet/app-user-profile/ion-content/ion-card/ion-card-content/ion-item[2]/ion-select"));
        System.out.println("El rol del usuario es: " + rolElement.getText());
        Thread.sleep(3000);
        rolElement.click();

        WebElement NewRolElement = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-alert/div[2]/div[3]/button[1]/div/div[2]"));
        NewRolElement.click();

        WebElement adminElement = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-alert/div[2]/div[4]/button[2]/span"));
        adminElement.click();
        Thread.sleep(3000);


        System.out.println("Nuevo el rol nuevo es: " + rolElement.getText());

    }

    //Enviar email

    @Test
    public void TestSendMail() throws InterruptedException{
        mainPage.toolsMenu.click();
        WebElement elemento = driver.findElement(By.xpath("//*[@id=\"footer\"]/ion-item[2]/a"));
        Thread.sleep(1000);
        elemento.click();

        WebElement smtpCard = driver.findElement(By.xpath("//ion-card-header[.//ion-card-title[text()='SERVIDOR SMTP']]"));

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", smtpCard);

        System.out.println("Testeando funcionalidad de .... " + smtpCard.getText());
        smtpCard.click();

        WebElement testButton = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-smtp-screen/ion-content/form/ion-card/ion-button[2]"));
        Thread.sleep(3000);
        testButton.click();

        System.out.println("Testeando funcionalidad de .... " + testButton.getText());
        Thread.sleep(3000);

        WebElement emailElement = driver.findElement(By.xpath("//*[@id='name2-id']"));
        emailElement.sendKeys("adavila59@ucatolica.edu.co");
        Thread.sleep(3000);

        WebElement adminElement = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-alert/div[2]/div[4]/button[2]/span"));
        adminElement.click();
        Thread.sleep(3000);
    }

    //Cambio de imágenes desde el módulo de administración.
    @Test
    public void testChangeImage() throws InterruptedException {
        mainPage.toolsMenu.click();
        WebElement elemento = driver.findElement(By.xpath("//*[@id=\"footer\"]/ion-item[2]/a"));
        Thread.sleep(1000);
        elemento.click();

        WebElement imageCard = driver.findElement(By.xpath("//ion-card-header[.//ion-card-title[text()='IMÁGENES']]"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", imageCard);

        System.out.println("Testeando funcionalidad de .... " + imageCard.getText());
        imageCard.click();

        WebElement imageElement = driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-storage/ion-content/ion-content/ion-card[1]/ion-card-content/ion-item/ion-input/input"));

        String filePath = System.getProperty("user.dir") + "\\src\\test\\java\\resources\\icon.png";
        System.out.println(filePath);
        imageElement.sendKeys(filePath);

        Thread.sleep(10000);
    }


}
